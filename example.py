from pygame.event import Event
import pygame
import ScreenObjects

class Theo(ScreenObjects.GuiObj):
    def __init__(self):
        super().__init__(r'theo.jpg')
    def onClick(self,event):
        if event.button == pygame.BUTTON_LEFT:
            print("I've been clicked!")
            print("-Theo")

class MainWindow(ScreenObjects.Window):
    def __init__(self, x: int, y: int, fps: int, bgc: tuple[int, int, int] = ..., screen: pygame.Surface = None) -> None:
        super().__init__(x, y, fps, bgc, screen)
        theo = Theo()
        self.clear()
        self.addToDisplay(theo,(10,10))
        self.updateDisplay()
        
    
    def keyPressed(self,event: Event):
        print(event.unicode)

        if event.key == pygame.K_LEFT:
            print('<-')
        if event.key == pygame.K_RIGHT:
            print('->')

        

x = MainWindow(1000,1000,30,(51,102,102))
x.switchToEvents()

